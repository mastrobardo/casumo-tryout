***Some notes about the test***

IMPORTANT: mobile only. Didnt even consider mediaqueries or other dimensions. Nobody should ever consider anythng else before completing mobile. Mobile is the king, not only for the users, but expecially to organise/design/setup the work to be done. Mobile first is a way of life nowadays :).

Also, i apologise for the delay, sadly with this big famiily of mine, im really, really lacking spare time, outside regular working hours (job is always priority). Also, not having time to complete milestones ( at least ), made everything slower ( Agile was not followed here, too many distractions )

***TECHS***
React, redux, Create react app. I used pure JS, and, as it emerged during forst interview, not being a fan of classes in JS (TS is different), only functional components. Love functional programming

***TESTING***
I started correcly with TDD ( at least view components were developed in TDD), but being late and struggling with free time i left testing out. While i know this is not production like behavior, i prioritize completing the test. Good practices take time at the beginning, to save time at the end of the workflow ( ie: less testing). Also, i didnt have time for e2e testing, which is usually time consuming. I'dlke to add some cypress testing, but there is no value in adding a test 'the page is opening'.

***DESIGNS***
Not 100% abiding. Having to uuse gimp for font sizes, colors and such ( im using invision or figma mostly nowadays ), and having to work on a crappy laptop with 8gb of RAM, i approximated a bit. However, theme.js is setup to be 100% compliant.

***LABELS***
Somthing i wanted to add, couldnt find time, so NO labels.json here ( in my job, we mostly use Polyglot )

***BAD PRACTICES***
arrow functions in listeners: not smtg i'd do on prod: even if, in this case, being mostly pure functions and chached by V8, it is still not something to be done on PROD.

***CREDIT CARD VALIDATION***
i create a crEdit card number validator function, following Luhn method, but i found out some bugs in my implementation. ( please check form.js and utils/creditCardValidation). In the end, i dd validate just the legth of credit card. I'd honestly use a 3rd party package for this in production, but it was against the rules. Also, there is no Visa/Master card validation, the types are just switched based on cardList legth in state. ( first master, then visa and so on)

***FORM INPUTS***
For forms im mostly using Formik in day-to-day job, as it is better to use dedicated, already existing solutions for forms ( no nedd to reinvent the wheel everytime). However, i respected the rules here, and use my components.

***SAVING DATA***
Just a middleware to save to localstorage. Not sure what i should have done here... honestly, state for users should be saved on Server, being the client just an output machine. Ayway, cards and state of the app are now saved on local storage

***ALSO IMPORTANT***
I duplicated the svg for the card backround. Changing SVG colors require transformations on the CSS side in a totally  unclear way ( hue/light/saturation). I always prefer to dont mess with the assets.
