import React from 'react';
import { connect, useDispatch } from 'react-redux';
import AppModalView from './appmodal.view';
import { saveCard, discardCard } from '../../redux/actions';

function AppModal(props) {
  const { selectedCard, error, cardList } = props;
  const dispatch = useDispatch();
  const isError = error && error.length > 0;
  const isEditing = selectedCard !== null ? cardList.filter(e => e.id === selectedCard.id).length > 0 : false;
  return (
    <AppModalView selectedCard={selectedCard} isEditing={isEditing}
      dispatch={isError ? () => { } : () => dispatch(saveCard())}
      onClose={() => dispatch(discardCard())}
    />
  )
}

const mapStateToProps = (state) => ({
  selectedCard: state.addEdit.selectedCard,
  cardList: state.addEdit.cardList,
  error: state.error.error
});

export default connect(mapStateToProps)(AppModal);
