import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import { styles } from './appmodal.styles';
import FormInput from '../forminput/forminput';
import { FORMS_ELEMENTS } from '../../constants/form'
import AppButton from '../appbutton/appbutton';
import Card from '../card/card.view'

function AppModalView(props) {
  const { closeIcon, container, wrapper, actions } = styles();
  const { selectedCard, dispatch, onClose, isEditing } = props;
  return (
    <Dialog open={Boolean(selectedCard)} aria-labelledby='form-dialog' className={container}>
      <IconButton className={closeIcon} data-testid='icon-close' onClick={onClose}>
        <CloseIcon />
      </IconButton>
      <DialogTitle data-testid='form-dialog-title'>Add card details</DialogTitle>
      <DialogContent data-testid='form-dialog-content' className={wrapper}>
        {
          isEditing ? <Card {...selectedCard} /> : ''
        }
        {
          FORMS_ELEMENTS.map((props, idx) => (<FormInput {...props} key={'input' + idx} value={selectedCard ? selectedCard[props.name] : ''} />))
        }
      </DialogContent>
      <DialogActions data-testid='form-dialog-actions' className={actions}>
        <AppButton text={'Save Card'} data-testid='AppButton' onClick={() => dispatch({ type: 'SAVE_CARD' })} />
      </DialogActions>
    </Dialog>
  )
}

export default AppModalView;
