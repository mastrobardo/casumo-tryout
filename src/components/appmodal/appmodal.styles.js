import { makeStyles } from '@material-ui/core/styles';

export const styles = makeStyles(theme => ({
  wrapper: {
    minWidth: '100%',
    padding: theme.spacing(4)
  },
  container: {
    width: '100% !important',
    padding: 0,
    margin: 0
  },
  closeIcon: {
    position: 'absolute',
    right: 0,
    width: 'auto',
    margin: theme.spacing(2),
    padding: 0
  },
  actions: {
    padding: theme.spacing(4)
  }

}));
