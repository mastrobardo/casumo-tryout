import React from 'react';
import { render, screen } from '@testing-library/react';
import AppModalView from './appmodal.view';
import { ThemeProvider } from '@material-ui/core';

import { theme } from './../../styles/theme/theme';

describe('AppModal View', () => {
  test('AppModalView - It renders its elements', () => {
    const { getByTestId } = render(
      <ThemeProvider theme={theme}>
        <AppModalView />
      </ThemeProvider>
    );

    const icon = getByTestId('icon-close');
    expect(icon).toBeInTheDocument();

    const title = getByTestId('form-dialog-title');
    expect(title).toBeInTheDocument();

    const cont = getByTestId('form-dialog-content');
    expect(cont).toBeInTheDocument();

    const actions = getByTestId('form-dialog-actions');
    expect(actions).toBeInTheDocument();
  })
})
