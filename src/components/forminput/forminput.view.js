import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputAdornment from '@material-ui/core/InputAdornment';
import TextField from '@material-ui/core/TextField';
import { ReactComponent as SuccessIcon } from '../../static/svg/form-success.svg';
import { ReactComponent as ErrorIcon } from '../../static/svg/form-error.svg';

const styles = makeStyles(theme => {
  return {
    input: {
      width: '100%',
      marginTop: theme.spacing(2)
    }
  }
})

function FormInputView(props) {
  const { type, label, errorText, isError, value, name, dispatch, validate, onChange } = props;
  const { input } = styles();
  let { onInput } = props;
  onInput = onInput || null;
  return (
    <div className={input}>
      <TextField value={value} type={type}
        onChange={e => onChange(validate, dispatch, name, e.currentTarget.value)}
        error={isError} helperText={isError ? errorText : ''} label={label} onInput={onInput} InputProps={{
          endAdornment: (
            <InputAdornment position='end'>
              {!isError ?
                <SuccessIcon data-testid={'success-icon'} /> :
                <ErrorIcon data-testid={'error-icon'} />}
            </InputAdornment>
          )
        }}>
        {value ? value : ''}
      </TextField>
    </div>
  )
}

export default FormInputView;
