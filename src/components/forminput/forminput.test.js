import React from 'react';
import { render } from '@testing-library/react';
import FormInputView from './forminput.view';

const props = {
  error: true
}
describe('FormInput View', () => {

  test('FormInputView - It renders an error', () => {
    const { getByTestId } = render(
      <FormInputView error={true} />
    );

    const error = getByTestId('error-icon');
    expect(error).toBeInTheDocument();
  })

  test('FormInputView - It renders an error', () => {
    props.error = false;
    const { getByTestId } = render(
      <FormInputView error={false} />
    );

    const success = getByTestId('success-icon');
    expect(success).toBeInTheDocument();
  })
})
