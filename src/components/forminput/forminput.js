import React from 'react';
import { useDispatch, connect } from 'react-redux';
import FormInputView from './forminput.view';
import { errorInCard, saveInput } from '../../redux/actions';

const onChange = (validate, dispatch, name, value) => {
  dispatch(errorInCard(name, validate(value)))
  dispatch(saveInput(name, value))
}

function FormInput(props) {
  const dispatch = useDispatch();
  const { error, name, value } = props;
  const isError = Boolean((error && error.indexOf(name) > -1)) || false;

  return (
    <FormInputView onChange={onChange} isError={isError} {...props} dispatch={dispatch} value={value} />
  )
}
const mapStateToProps = (state, ownProps) => ({
  error: state.error.error,
  selectedCard: state.selectedCard,
  // value: state.selectedCard[ownProps.name]
});

export default connect(mapStateToProps)(FormInput);
