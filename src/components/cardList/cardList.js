import React from 'react';
import { connect, useDispatch } from 'react-redux';
import CardView from '../card/card.view';
import { makeStyles } from '@material-ui/core/styles';
import { editCard } from '../../redux/actions';

const styles = makeStyles({
  container: {
    height: '70vh',
    overflow: 'scroll',
  }
});

function CardList(props) {
  const { cardList } = props;
  const { container } = styles();
  const dispatch = useDispatch();

  return (
    <div className={container}>
      {
        cardList.map((props) => {
          return <CardView {...props} key={props.id} dispatch={() => dispatch(editCard(props.id))} />
        })
      }
    </div>
  )
}

const mapStateToProps = (state /* , ownProps */) => ({
  cardList: state.addEdit.cardList,
});

export default connect(
  mapStateToProps,
  null,
)(CardList);
