import React from 'react';
import { render, screen } from '@testing-library/react';
import HeaderView from './header.view';
import { ThemeProvider } from '@material-ui/core';

import { theme } from './../../styles/theme/theme';

describe('Header View', () => {
  test('HeaderView - It renders its elements', () => {
    const { getByTestId } = render(
      <ThemeProvider theme={theme}>
        <HeaderView />
      </ThemeProvider>
    );

    const title = getByTestId('header-title');
    expect(title).toBeInTheDocument();

    const subtitle = getByTestId('header-subtitle');
    expect(subtitle).toBeInTheDocument();
  })
})
