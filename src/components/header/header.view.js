import React from 'react';
import { withTheme, makeStyles } from '@material-ui/core/styles';

const styles = makeStyles(theme => ({
  title: {
    ...theme.typography.h1,
    color: theme.palette.primary.light
  },
  subtitle: {
    ...theme.typography.body1,
    color: theme.palette.grey[50]
  }
}))

function HeaderView() {
  const { title, subtitle } = styles();
  return (
    <div data-testid='header'>
      <h1 className={title} data-testid='header-title'>YOUR CARDS</h1>
      <h4 className={subtitle} data-testid='header-subtitle'>Add, edit or delete at any time</h4>
    </div>
  )
}

export default withTheme(HeaderView)
