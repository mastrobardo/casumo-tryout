import { makeStyles } from '@material-ui/core/styles';
import CardBackgroundShapeMaster from './../../static/svg/card-background-shape-master.svg';
import CardBackgroundShapeVisa from './../../static/svg/card-background-shape-visa.svg';

export const dedicatedCardStyles = (type) => {
  // assuming only 2 types without Emex and American Express, concept would be the same
  // with omly more options
  const rgba = type === 'master' ? 'rgb(59,5,142)' : 'rgb(1,201,199)';
  const bgShape = type === 'master' ? CardBackgroundShapeMaster : CardBackgroundShapeVisa;
  return makeStyles(theme => ({
    cardBackground: {
      background: `url('${bgShape}') ${rgba}`,
      backgroundRepeat: 'no-repeat',
      backgroundPosition: 'bottom right',
      backgroundSize: 'contain'
    },
  }))()
};

export const styles = makeStyles(theme => ({
  wrapper: {
    width: '100%',
    paddingTop: '50%', //`${((182 * 100) / 310)}%`, //abide to ratio
    position: 'relative',
    padding: theme.spacing(3),
    borderRadius: theme.radiuses.primary,
  },
  container: {
    maxWidth: '100%',
    margin: `${theme.spacing(2)}px 0`
  },
  logo: {
    top: 0,
    left: 0
  },
  innerBox: {
    position: 'absolute',
    padding: theme.spacing(3),
  },
  info: {
    top: 0,
    right: 0,
    display: 'flex',
    justifyContent: 'space-around'
  },
  infoBoxes: {
    display: 'flex',
    flexDirection: 'column',
    '&&:nth-child(2)': {
      marginLeft: theme.spacing(3),
    },
  },
  infoLabel: {
    fontSize: 8,
    fontWeight: 'bold',
    color: theme.palette.grey[50]
  },
  infoLabelContent: {
    fontSize: 14,
    fontWeight: 'bold',
    color: theme.palette.common.white
  },
  cardNumber: {
    bottom: 0,
    paddingLeft: 0,
    display: 'flex',
    flexDirection: 'column',
  },
  cardNumberContent: {
    fontSize: 14,
    color: theme.palette.grey[50]
  },
  cardNumbersChuncks: {
    '&&:not(:first-child)': {
      marginLeft: theme.spacing(3),
    }
  }
}));
