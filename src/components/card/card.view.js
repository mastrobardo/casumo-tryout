import React from 'react';
import { withTheme } from '@material-ui/core/styles';

//assets
import { ReactComponent as MasterCardLogo } from './../../static/svg/mastercard-logo.svg';
import { ReactComponent as VisaLogo } from './../../static/svg/visa-logo.svg';

//own
import { styles, dedicatedCardStyles } from './card.styles';

function CardView(props) {

  const { type, number, cvc, expiry, name, dispatch } = props;
  const { container, wrapper, logo, innerBox, info, infoBoxes,
    infoLabel, infoLabelContent, cardNumber, cardNumbersChuncks } = styles();
  const { cardBackground } = dedicatedCardStyles(type);
  let onClick = dispatch ? () => dispatch() : () => { }
  return (
    <div className={container} data-testid='card-container' onClick={onClick}>
      <div className={`${wrapper} ${cardBackground}`} data-testid='card-wrapper'>
        <div className={`${logo} ${innerBox}`} data-testid='card-logo'>
          {
            type === 'master'
              ? <MasterCardLogo data-testid='card-logo-master' />
              : <VisaLogo data-testid='card-logo-visa' />
          }
        </div>

        <div className={`${info} ${innerBox}`} data-testid='card-info'>
          <div className={infoBoxes}>
            <span className={infoLabel}>CVC</span>
            <span className={infoLabelContent}>{cvc}</span>
          </div>
          <div className={infoBoxes}>
            <span className={infoLabel}>Expiry</span>
            <span className={infoLabelContent}>{expiry}</span>
          </div>
        </div>

        <div className={`${cardNumber} ${innerBox}`} data-testid='card-holder'>
          <span data-testid='card-holder-name' className={infoLabelContent}>{name}</span>
          <div data-testid='card-holder-numbers'>
            {(number &&
              <span className={cardNumbersChuncks}>{number}</span>
            )}
          </div>
        </div>
      </div>

      {/*

      <div className='edit'></div> */}
      {/* <CardBackgroundShape /> */}
    </div>
  )
}

export default withTheme(CardView);
