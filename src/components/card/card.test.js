import React from 'react';
import { render, screen } from '@testing-library/react';
import CardView from './card.view';
import { ThemeProvider } from '@material-ui/core';

import { theme } from './../../styles/theme/theme'

const props = {
  type: 'master',
  numbers: [123, 456, 789, 157],
  name: 'Davide',
  cvc: '123',
  expiry: '20/12'

}

describe('Card View', () => {
  test('CardView - It renders its elements', () => {
    const { getByTestId } = render(
      <ThemeProvider theme={theme}>
        <CardView {...props} />
      </ThemeProvider>
    );

    const logo = getByTestId('card-logo');
    expect(logo).toBeInTheDocument();

    const infos = getByTestId('card-info');
    expect(infos).toBeInTheDocument();

    const holder = getByTestId('card-holder');
    expect(holder).toBeInTheDocument();
  })

  test('CardView - It renders a different logo', () => {
    const { getByTestId } = render(
      <ThemeProvider theme={theme}>
        <CardView type="visa" />
      </ThemeProvider>
    );

    const logo = getByTestId('card-logo-visa');
    expect(logo).toBeInTheDocument();
  })
})
