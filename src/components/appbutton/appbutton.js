import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Button } from '@material-ui/core';

const styles = makeStyles(theme => ({
  button: {
    ...theme.typography.button.primary,
    color: theme.palette.common.white,
    backgroundColor: theme.palette.primary.light,
    '&:hover': {
      backgroundColor: theme.palette.primary.dark,
    },
    borderRadius: theme.radiuses.ternary,
    padding: `${theme.spacing(2)}px 0`,
    margin: '0 auto',
    width: '100%',
  }
}))

function AppButton(props) {
  const { text, onClick } = props;
  const { button } = styles();
  return (
    <Button className={button} data-testid='button' onClick={onClick}>{text}</Button>
  )
}

export default AppButton;
