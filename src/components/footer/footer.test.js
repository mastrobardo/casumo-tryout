import React from 'react';
import { render } from '@testing-library/react';
import { ThemeProvider } from '@material-ui/core';
import FooterView from './footer.view';
import { theme } from '../../styles/theme/theme';

describe('Footer View', () => {
  test('FooterView - It renders', () => {
    const { getByTestId } = render(
      <ThemeProvider theme={theme}>
        <FooterView />
      </ThemeProvider>
    );

    const button = getByTestId('button');
    expect(button).toBeInTheDocument();
  })
})
