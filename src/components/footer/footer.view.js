import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppButton from '../appbutton/appbutton'

const styles = makeStyles(theme => ({
  container: {
    position: 'absolute',
    bottom: 0,
    width: `calc(100% - ${theme.spacing(4) * 2}px)`,
    padding: `${theme.spacing(3)}px 0`,
  }
}))

function Footer(props) {
  const { container } = styles();
  const { dispatch } = props;
  return (
    <div className={container}>
      <AppButton data-testid='button' text={'Add new card'} onClick={() => dispatch({ type: 'NEW_CARD' })} />
    </div>
  )
}

export default Footer;
