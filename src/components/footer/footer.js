import React from 'react';
import { useDispatch, connect } from 'react-redux';
import FooterView from './footer.view';

function Footer() {
  const dispatch = useDispatch();
  return (
    <FooterView dispatch={dispatch} />
  )
}

export default connect()(Footer);
