import React from 'react';
import { withTheme } from '@material-ui/core/styles';
import HeaderView from './components/header/header.view';
import Footer from './components/footer/footer';
import CssBaseline from '@material-ui/core/CssBaseline';
import CardList from './components/cardList/cardList';
import AppModal from './components/appmodal/appmodal';
import { connect } from 'react-redux';

function AppRaw(props) {
  const { cardList } = props;
  return (
    <React.Fragment>
      <div className="App" style={{ padding: 32 }}>
        <CssBaseline />
        <HeaderView />
        <CardList cardList={cardList} />
        <Footer />
      </div>
      <AppModal />
    </React.Fragment>
  )
}

const mapStateToProps = (state /*, ownProps*/) => {
  return {
    cardList: state.addEdit.cardList,
    selectedCard: state.addEdit.selectedCard
  }
}

export default connect(
  mapStateToProps
)(withTheme(AppRaw))
