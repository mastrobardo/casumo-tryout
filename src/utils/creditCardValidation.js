const luhnCheck = (val) => {
  let sum, i = 0;
  for (i = 0; i < val.length; i++) {
    let intVal = parseInt(val.substr(i, 1));
    if (i % 2 === 0) {
      intVal *= 2;
      if (intVal > 9) {
        intVal = 1 + (intVal % 10);
      }
    }
    sum += intVal;
  }
  return (sum % 10) === 0;
}

function validateCardNumber(number) {
  let regex = new RegExp("^[0-9]{16}$");
  if (!regex.test(number))
    return false;

  return luhnCheck(number);
}

export default validateCardNumber;
