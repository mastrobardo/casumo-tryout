import { applyMiddleware, createStore } from 'redux';
import { createLogger } from 'redux-logger';
import thunkMiddleware from 'redux-thunk';
import reducers from './reducers';
import { save, load } from "redux-localstorage-simple"

const loggerMiddleware = createLogger();

// export const store = createStore(
//   reducers,
//   applyMiddleware(
//     thunkMiddleware,
//     // loggerMiddleware
//   )
// );

const createStoreWithMiddleware
  = applyMiddleware(
    thunkMiddleware,
    save()
  )(createStore)

export const store = createStoreWithMiddleware(
  reducers,
  load() 
)
