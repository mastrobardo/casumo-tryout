export const NEW_CARD = 'NEW_CARD'
export function newCard() {
  return {
    type: NEW_CARD,
  }
}

export const EDIT_CARD = 'EDIT_CARD'
export function editCard(cardId) {
  return {
    type: EDIT_CARD,
    cardId
  }
}

export const SAVE_CARD = 'SAVE_CARD'
export function saveCard() {
  return {
    type: SAVE_CARD
  }
}

export const DISCARD_CARD = 'DISCARD_CARD'
export function discardCard() {
  return {
    type: DISCARD_CARD
  }
}

export const ERROR = 'ERROR';
export function errorInCard(name, isValid) {
  return {
    type: ERROR,
    error: name,
    isValid
  }
}

export const SAVE_CARD_INPUT = 'SAVE_CARD_INPUT'
export function saveInput(name, value) {
  return {
    type: SAVE_CARD_INPUT,
    key: name,
    value
  }
}
