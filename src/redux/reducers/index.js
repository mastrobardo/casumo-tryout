import { combineReducers } from 'redux';
import addEdit from './add-edit';
import error from './error';

export default combineReducers({
  addEdit,
  error
})
