
export default function error(state = { error: ['name', 'expiry', 'number', 'cvc'] }, action) {
  let error;
  switch (action.type) {
    case 'ERROR':
      if (!action.isValid) {
        const errorCopy = (state.error && state.error.concat()) || [];
        const newError = errorCopy.filter(e => e === action.error).length < 1 ? errorCopy.push(action.error) : false;
        error = newError ? errorCopy : state.error;
      } else {
        error = (state.error && state.error.concat()) || [];
        error = error.filter(e => e !== action.error)
      }
      return Object.assign({}, state, { error: error });
    case 'NEW_CARD':
      return Object.assign({}, state, { error: ['name', 'expiry', 'number', 'cvc'] });
    case 'EDIT_CARD':
      return Object.assign({}, state, { error: [] });
    default:
      return state;
  }
}
