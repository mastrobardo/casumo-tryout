import { v4 as uuidv4 } from 'uuid';

export default function addEdit(state = { cardList: [], selectedCard: null }, action) {
  let selectedCard;
  switch (action.type) {
    case 'NEW_CARD':
      const newCard = {
        id: uuidv4(),
        name: '',
        number: '',
        expiry: '',
        cvc: ''
      }
      return Object.assign({}, state, { selectedCard: newCard });
    case 'EDIT_CARD':
      selectedCard = state.cardList.concat().filter(e => e.id === action.cardId)[0];
      return Object.assign({}, state, { selectedCard: selectedCard });
    case 'DISCARD_CARD':
      return Object.assign({}, state, { selectedCard: null });
    case 'SAVE_CARD':
      selectedCard = state.selectedCard;
      let cardList = state.cardList.concat();
      const isEdit = cardList.filter(e => e.id === selectedCard.id).length;
      if (isEdit) {
        cardList = cardList.filter(e => e.id !== selectedCard.id)
      }
      selectedCard.type = cardList.length % 2 === 0 ? 'master' : 'visa';
      cardList.push(selectedCard);
      return Object.assign({}, state, { selectedCard: null, cardList });
    case 'SAVE_CARD_INPUT':
      selectedCard = Object.assign({}, state.selectedCard);
      selectedCard[action.key] = action.value;
      return Object.assign({}, state, { selectedCard: selectedCard });
    default:
      return state;
  }
}
