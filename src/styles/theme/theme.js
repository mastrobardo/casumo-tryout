import { createMuiTheme, responsiveFontSizes } from '@material-ui/core/styles';
import LineToCircularWoff2 from './../../static/fonts/lineto-circular-pro-book.woff2';
import LineToCircularWoff from './../../static/fonts/lineto-circular-pro-book.woff';
import LineToCircularTtf from './../../static/fonts/lineto-circular-pro-book.ttf';

const lineToCircular = {
  fontFamily: 'LineToCircular',
  fontStyle: 'normal',
  fontDisplay: 'swap',
  fontWeight: 400,
  src: `
    local('LineToCircular'),
    local('LineToCircular-Regular'),
    url(${LineToCircularWoff2}) format('woff2')
    url(${LineToCircularWoff}) format('woff')
    url(${LineToCircularTtf}) format('true-type')
  `,
  unicodeRange:
    'U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF',
};

// const lineToCircularBold = {
//   fontFamily: 'LineToCircular',
//   fontStyle: 'normal',
//   fontDisplay: 'swap',
//   fontWeight: 700,
//   src: `
//     local('LineToCircular'),
//     local('LineToCircular-Bold'),
//     url(${LineToCircularWoff2}) format('woff2')
//   `,
//   unicodeRange:
//     'U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF',
// };

const themeConfig = {
  palette: {
    primary: {
      light: '#4C00C2',
      dark: '#32007E',
      main: '#3B058E'
    },
    grey: {
      main: '#E5E5E5',
      20: '#D3D8E1',
      50: '#798291',
      70: '#444E5D',
      90: '#1A212C',
    },
    success: {
      main: '#19AC51'
    },
    error: {
      main: '#FC484C'
    }
  },
  typography: {
    fontFamily: [
      'LineToCircular',
      'Arial'
    ].join(','),
    body1: {
      fontSize: 12,
      fontWeight: 400
    },
    button: {
      primary: {
        fontSize: 14
      },
      secondary: {
        fontSize: 16
      }
    },
    h1: {
      fontSize: 30,
      fontWeight: 700
    },
    h2: {
      fontSize: 24
    }
  },
  radiuses: {
    primary: 16,
    secondary: 24,
    ternary: 100
  },
  spacing: 8,
  overrides: {
    MuiCssBaseline: {
      '@global': {
        '@font-face': [lineToCircular],
      },
    },
    MuiDialog: {
      paper: {
        width: '100%',
        margin: 0,
        borderRadius: 16
      }
    },
    MuiTextField: {
      root: {
        width: '100%'
      }
    },
  },
}

export const theme = responsiveFontSizes(createMuiTheme(themeConfig));
