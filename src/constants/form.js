import validateCardNumber from '../utils/creditCardValidation'
export const FORMS_ELEMENTS = [
  {
    type: 'text',
    name: 'name',
    label: 'Name on Card',
    errorText: 'Please fill in your name',
    validate: (value) => {
      return value.length > 5
    }
  },
  {
    type: 'tel',//honestly speaking i dont understand why in 2020 we dont have a dedicated type for this
    name: 'number',
    label: 'Card Number',
    errorText: 'Please enter a valid card number',
    validate: (value) => {
      return value.length === 19
      // return validateCardNumber(value)
    },
    onInput: e => e.target.value = e.target.value.replace(/[^\dA-Z]/g, '').replace(/(.{4})/g, '$1 ').trim()
  },
  {
    type: 'text',
    name: 'expiry',
    label: 'Expiry Date',
    errorText: 'Please enter a valid expiry date',
    onInput: (e) => {
      return e.target.value = e.target.value.replace(
        /[^0-9]/g, ''
      ).replace(
        /^([2-9])$/g, '0$1'
      ).replace(
        /^(1{1})([3-9]{1})$/g, '0$1/$2'
      ).replace(
        /^0{1,}/g, '0'
      ).replace(
        /^([0-1]{1}[0-9]{1})([0-9]{1,2}).*/g, '$1/$2'
      );
    },
    validate: (value) => value.length === 5
  },
  {
    type: 'tel',
    name: 'cvc',
    label: 'CVC (security code)',
    errorText: 'Please enter a valid expiry date',
    validate: (value) => value.length === 3
  },
];

